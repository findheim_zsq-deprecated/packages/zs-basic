pRequire = require 'parent-require'
gulp = pRequire "gulp"
$ = require('gulp-load-plugins')
  pattern: ['gulp-*']

module.exports = (options)->
  htmlSrc = ["node_modules/web-basic/map/index.html"]
  coffeeBaseSrc = ["node_modules/web-basic/map/code/base/**/*.coffee"]
  coffeeStaticSrc = ["node_modules/web-basic/map/code/static.coffee"]
  jsLibSrc = ["node_modules/web-basic/map/lib/**/*.js"]
  sassSrc = ["node_modules/web-basic/map/style/**/*.scss"]
  cssLibSrc = ["node_modules/web-basic/map/lib/**/*.css"]

  gulp.task "map-html", ()->
    gulp.src htmlSrc
    .pipe gulp.dest("#{options.dest.root}/map")

  gulp.task "map-scripts", ()->
    gulp.src coffeeBaseSrc
    .pipe $.addSrc.append coffeeStaticSrc
    .pipe $.coffeelint()
    .pipe $.coffeelint.reporter()
    .pipe $.coffee({bare: true}).on 'error', (err)-> options.errorHandler("Compile coffeescript", err)
    .pipe $.addSrc.prepend jsLibSrc
    .pipe $.concat('script.js')
    .pipe $.uglify().on 'error', (err)-> options.errorHandler("Minify JS", err)
    .pipe gulp.dest("#{options.dest.root}/map")
    
  gulp.task "map-style", ()->
    gulp.src sassSrc
    .pipe $.sass({errLogToConsole: true,outputStyle: 'expanded'}).on 'error', (err)-> options.errorHandler("Compile sass", err)
    .pipe $.addSrc.prepend cssLibSrc
    .pipe $.autoprefixer({browsers: ['last 2 versions']}).on 'error', (err)-> options.errorHandler("Autoprefix css", err)
    .pipe $.concat('style.css')
    .pipe $.minifyCss().on 'error', (err)-> options.errorHandler("Minify css", err)
    .pipe gulp.dest("#{options.dest.root}/map")

  gulp.task "map", ["map-html", "map-scripts", "map-style"]
  
  