getQueryParam = (name)->
  regex = new RegExp "[?&]" + name + "=([^&#]*)"
  results = regex.exec(location.search)
  if results?
    return decodeURIComponent(results[1])
  return null

init = ->
  extId = getQueryParam 'extId'
  window.zsMap.country = getQueryParam 'country'
  if extId? and extId.length > 3
    zsMap.estate.loadByExtId extId

zsMap.create("#j-map")
init()