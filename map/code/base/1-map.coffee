mapconfig =
  BDM_ENDPOINT: 'https://metadata.zoomsquare.com'

class Map
  constructor: ()->
    @loading = []
    @spinner = document.querySelector '.j-loading'

  startLoading: (key)->
    if @loading.indexOf(key) == -1
      @loading.push key
    classie.remove @spinner, 'hidden'

  stopLoading: (key)->
    index = @loading.indexOf(key)
    if index != -1
      @loading.splice index, 1
    if @loading.length == 0
      classie.add @spinner, 'hidden'

  create: (selector, opts) ->
# somehow the default path gets lost. fix it do bdm for now.
    L.Icon.Default.imagePath = mapconfig.BDM_ENDPOINT + "/static/assets/leaflet/images"

    ele = document.querySelector(selector)
    unless ele?
      console?.log "Map Init failed: cannot find element with selector #{selector}"
      return

    unless opts?
      opts = {}

    unless opts.language in ['de','en']
      opts.language = 'de'
    layerNames =
      de: {map:"Karte",aerial:"Luftbild",halloween:"Halloween"}
      en: {map:"Map",aerial:"Aerial Map",halloween:"Halloween"}
    attribution = 
      de: "Lagen von zoomsquare geschätzt"
      en: "Locations estimated by zoomsquare"

    tileLayers = {}
    addTileLayer = (text, key)=>
      map = new L.TileLayer "https://api.mapbox.com/v4/#{key}/{z}/{x}/{y}.png?access_token=pk.eyJ1Ijoiem9vbXNxdWFyZSIsImEiOiJiYTFhYmZlODkwNzQwZTc1OGQxMThjMTU1NzU1ZmM2ZSJ9.2eXIKouulITlZRWPCQRgkA", 
        detectRetina: true
        attribution: "© <a href='https://www.mapbox.com/map-feedback/' target='_blank'>Mapbox</a> © <a href='https://www.openstreetmap.org/copyright' target='_blank'>OSM contributors</a> | #{attribution[opts.language]}"
      map.on 'loading', ()=>
        @startLoading 'tiles-'+key
      map.on 'load', ()=>
        @stopLoading 'tiles-'+key
      tileLayers[text] = map
      return map

    baseMap = addTileLayer layerNames[opts.language]["map"], "mapbox.streets"
    addTileLayer layerNames[opts.language]["aerial"], "mapbox.streets-satellite"
    #addTileLayer layerNames[opts.language]["halloween"], "duncangraham.c8178788"

    if opts.bbox
      bboxBounds = L.latLngBounds(opts.bbox)
      mapCenter = bboxBounds.getCenter()
      mapZoom = @_getZoomLevel ele.offsetWidth, bboxBounds

    curmap = L.map ele,
      center: mapCenter or opts.center or [48.2, 16.367]
      zoom: mapZoom or opts.zoom or 12
      minZoom: 7,
      maxZoom: 17,
      maxBounds: opts.maxBounds or [[46.07, 5.56],[55.35, 17.47]] #[[46.37, 5.86],[55.05, 17.17]]
      zoomControl: false #custom on right side
      attributionControl: false

    #if opts.bbox?.length > 1
    #  southWest = L.latLng(opts.bbox[0])
    #  northEast = L.latLng(opts.bbox[1])
    #  bounds = L.latLngBounds(southWest, northEast)
    #  curmap.fitBounds(bounds)

    curmap.addLayer baseMap
    L.control.attribution({position: "bottomright"}).addTo curmap
    L.control.zoom({position: "topright"}).addTo curmap
    L.control.scale({imperial: false, position: "bottomleft"}).addTo curmap
    L.control.layers(tileLayers, {}, {collapsed: true, position: "bottomright"}).addTo curmap

    # locate
    if opts.panTo && google?.loader?.ClientLocation?.latitude?
      curmap.panTo [ google.loader.ClientLocation.latitude, google.loader.ClientLocation.longitude ]
    #log.debug "map init complete"
    return curmap

  _getZoomLevel: (elWidth, bboxBounds)->
    baseWidth = 600
    zoomTable = [
      {'zoom': 7, 'value': 6.591797}
      {'zoom': 8, 'value': 3.295899}
      {'zoom': 9, 'value': 1.647950}
      {'zoom': 10, 'value': 0.823975}
      {'zoom': 11, 'value': 0.411988}
      {'zoom': 12, 'value': 0.205994}
      {'zoom': 13, 'value': 0.102997}
      {'zoom': 14, 'value': 0.051499}
      {'zoom': 15, 'value': 0.025750}
      {'zoom': 16, 'value': 0.012875}
      {'zoom': 17, 'value': 0.006438}
    ]

    widthRatio = elWidth/baseWidth
    bboxWidth = bboxBounds.getEast() - bboxBounds.getWest()

    for z in zoomTable.reverse()
      a = widthRatio * z.value
      if a >= bboxWidth
        return z.zoom
    return 7

window.zsMap = {}
window.zsMap.create = (elementId, options)->
  window.zsMap.helper = new Map()
  window.zsMap.map = zsMap.helper.create elementId, options