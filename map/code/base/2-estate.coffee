class Estate
  estateLayer = null

  drawGeo: (geo, options)->
    options = {} unless options?
    if @estateLayer?
      @removeGeo()
    @estateLayer = L.geoJson geo.geo
    if options.disablePopup != false
      if geo.precision == 'point'
        latLng = {lat: geo.lat, lng: geo.lng}
        offset = [0,-50]
      else
        latLng = {lat: geo.bbox[1][1], lng: geo.bbox[0][0] + (geo.bbox[1][0] - geo.bbox[0][0]) / 2}
        offset = [0,-10]
      popup = L.popup {closeButton: false, closeOnClick: false, autoPan: false, offset: offset, className: 'map-locationPopup'}
      .setLatLng latLng
      .setContent geo.label
      @estateLayer.on 'mouseover', ()->
        popup.openOn(zsMap)
      @estateLayer.on 'mouseout', ()->
        zsMap.map.closePopup()
    zsMap.map.addLayer @estateLayer
    if options.fitBounds != false
      zsMap.map.fitBounds(@estateLayer.getBounds().pad(0.3), {animate:false, reset:true}) #, maxZoom: settings.map.maxZoom

  removeGeo: ()->
    if @estateLayer?
      zsMap.map.removeLayer @estateLayer
      @estateLayer = null

  loadByExtId: (extId)->
    zsMap.helper.startLoading 'singleEstate'
    request = new XMLHttpRequest()
    countryParam = if zsMap.country? then "?country=#{zsMap.country}" else ""
    request.open("GET","/api/map/getGeo/#{extId}#{countryParam}",true)
    request.onreadystatechange = ()=>
      if request.readyState == 4 && request.status == 200
        @drawGeo JSON.parse(request.responseText)
        zsMap.helper.stopLoading 'singleEstate'
    request.send()

window.zsMap.estate = new Estate()