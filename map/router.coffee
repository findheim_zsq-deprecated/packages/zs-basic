webapp = require 'zs-webapp/lib/webapp'
q = require 'q'
utils = require './../server/utils'
cfg = require 'zs-app/lib/config'
request = require 'request'

router = webapp.newRouter()
resCfg = cfg.get('res')
estateEndpoint = "#{resCfg.url}#{resCfg.endpoints.estate}"
if resCfg.auth.user? && resCfg.auth.pass?
  estateEndpoint += "?user=#{resCfg.auth.user}&pass=#{resCfg.auth.pass}"

getGeo = (extId, country)->
  def = q.defer()
  unless country?
    country = 'at'
  request
    uri: estateEndpoint
    method: "POST"
    json: true
    body:
      query:
        _meta:
          includeKeys: ["geoResult"]
          backend: resCfg.meta.backend
          index: country
        extIds: [extId]
  , (err,msg,body)->
    if err?
      def.reject err
    else if body.estates.length != 1
      def.reject new Error("invalid extId")
    else
      geo = body.estates[0].geoResult
      if geo.precision is 'georef'
        request
          uri: cfg.get('bdm').url + "/api/getGeoEntity/#{geo.id}?geo"
          method: "GET"
          json: true
        , (err,msg,body)->
          if err?
            def.reject err
          else
            def.resolve
              id: body.id
              label: body.label
              lat: body.lat
              lng: body.lng
              bbox: body.bbox
              precision: body.precision
              geo: body.geo
      else
        def.resolve
          id: geo.id
          label: geo.label
          lat: geo.lat
          lng: geo.lng
          bbox: geo.bbox
          precision: geo.precision
          geo: geo.geo
  def.promise

router.get '/getGeo/:extId', (req, result)->
  getGeo utils.decodeExtId(req.params.extId), req.query.country
  .then (geo)->
    result.json geo
  .catch (err)->
    result.status(500).send(err.message)

module.exports = router