env = require 'zs-app/lib/env'
module.exports =
  servers:
    statistics:
      url: "https://statistics.zoomsquare.com"
      endpoints:
        zoomlabs: "/api/zoomlabs"
        tubemap: "/api/tubemap"
        history: "/api/history"
        profiles: "/api/profiles"
      auth:
        user: 'zoomsquare'
        pass: env.get('ZS_STATISTICS_PASSWORD', null)
        
    mailService:
      url: "https://mailservice.zoomsquare.com"
      endpoints:
        sendUser: "/send/user"
        sendPerson: "/send/person"
        sendWhitelist: "/send/whitelist"
      auth:
        user: 'zoomsquare'
        pass: env.get('ZS_MAILSERVICE_PASSWORD', null)
  languages: ["de","en"]
  bdm:
    url: "https://metadata.zoomsquare.com"
  res:
    url: env.get "ZS_WEB3_RES_URL", "http://res.zoomsquare.com:5000"
    endpoints:
      search: "/search/v1"
      estate: "/estate/v1"
      extIds: "/estate/v1/extIds"
      ad: "/ad/v1"
      v2:
        search: "/search/v2"
        estate: "/estate/v2"
        extIds: "/estate/v2/extIds"
        similars: "/similars/v2"
        agentEstates: "/agentestates/v2"
        premiumEstates: "/premiumestates/v2"
    auth:
      user: 'zoomsquare'
      pass: env.get('ZS_WEB3_RES_PASSWORD', null)
    meta:
      backend: env.get "ZS_WEB3_RES_META_BACKEND", "ES"
  userService:
    base: env.get "ZS_WEB3_USER_SERVICE_URL", "https://userservice.zoomsquare.com"
    user: 'zoomsquare'
    pass: env.get('ZS_WEB3_USER_SERVICE_PASSWORD', null)
  facebook:
    client_id: "233796170055369"
    client_secret: "b9a80bb3c1d6ecf1bbc5cea6622fee13"
    endpoints:
      accessToken: 'https://graph.facebook.com/v2.3/oauth/access_token'
      me: 'https://graph.facebook.com/v2.3/me'
  rootUrls:
    at: env.get 'ZS_ROOT_URL_AUSTRIA', 'https://www.zoomsquare.com/'
    de: env.get 'ZS_ROOT_URL_GERMANY', 'https://www.zoomsquare.de/'
  imgProxy: "https://thumbs.zoomsquare.com/"
  infinity: Math.pow 2,30
  countries:
    at:
      port: 0
      topDomain: 'com'
      domain: 'zoomsquare.com'
      rootUrl: 'https://www.zoomsquare.com/'
      email: 'hello@zoomsquare.com'
      language: 'de'
      de:
        countryName: 'Österreich'
      en:
        countryName: 'Austria'
    de:
      port: 1
      topDomain: 'de'
      domain: 'zoomsquare.de'
      rootUrl: 'https://www.zoomsquare.de/'
      email: 'hallo@zoomsquare.de'
      language: 'de'
      de:
        countryName: 'Deutschland'
      en:
        countryName: 'Germany'
