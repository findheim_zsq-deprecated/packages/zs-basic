# Common Templates

Common frontend template code, which is used in different zs projects
templates will be provided in handlebar with the name "basic/FOLDERNAME"

## Cookiebar

### Usage
Call `window.zsCookiebar.check()` on Client. This will show the cookiebar, if the user hasn't agreed already.  
Agreement of the user is stored in localStorage with the key `persistent.cookiesAgreed`.

### Requirements
- Translations


## Footer

### Usage
Just add the template, where you want the footer to be included in your page.

### Requirements
- handlebars Templating
    - `translate` function
    - `langPrefix` ("", "/en")
    - `currentRoute` (current route without language prefix, eg: "/vision" or "/team")