#these tracking links are required by the shared templates
#this also do not initiate tracking, it just makes easier handable links.
window.trackEvent = (category, action, label, cb)->
  if ga?
    ga "send", "event", category, action, label,
      'hitCallback': ()->
        if cb?
          cb()
  else if cb?
    setTimeout cb, 0


window.trackLink = (elem, category, action, label)->
  if ga?
    ga "send", "event", category, action, label,
      'hitCallback': ()->
        document.location = elem.getAttribute('href')
    return false
  else
    return true