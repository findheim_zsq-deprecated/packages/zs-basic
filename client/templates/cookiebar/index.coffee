class ZsCookiebar

  check: () ->
    unless window.localStorage.getItem('persistent.cookiesAgreed') == "true"

      cookiebarElement = document.createElement 'div'
      classie.add cookiebarElement, 'cookiebar'
      cookiebarElement.innerHTML = document.querySelector('#j-cookiebar').textContent

      cookiebarButtonElement = document.createElement 'a'
      classie.add cookiebarButtonElement, 'button'
      classie.add cookiebarButtonElement, 'cookiebar__button'
      cookiebarButtonElement.innerHTML = 'Ok'

      cookiebarElement.appendChild cookiebarButtonElement
      document.body.appendChild cookiebarElement

      cookiebarButtonElement.addEventListener 'click', ->
        window.localStorage.setItem 'persistent.cookiesAgreed', "true"
        document.body.removeChild cookiebarElement

window.zsCookiebar = new ZsCookiebar()