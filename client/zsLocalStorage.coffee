class ZsLocalStorageOverride
  constructor: ()->
    unless @checkLocalStorage()
      @zsLocalStorage = {}
      Storage.prototype.getItem = @getItem
      Storage.prototype.setItem = @setItem
      Storage.prototype.removeItem = @removeItem

  checkLocalStorage: ()->
    try
      localStorage.setItem 'zsLocalStorageTest', 'zsTest'
      if localStorage.getItem('zsLocalStorageTest') == 'zsTest'
        localStorage.removeItem('zsLocalStorageTest')
        return true
      return false
    catch e
      return false

  setItem: (name, value)=>
    @zsLocalStorage[name] = value.toString()

  getItem: (name)=>
    @zsLocalStorage[name] or undefined

  removeItem: (name)=>
    delete @zsLocalStorage[name]
new ZsLocalStorageOverride()