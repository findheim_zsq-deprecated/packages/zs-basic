window.zs = window.zs || {}

document.addEventListener "DOMContentLoaded", ()->
  langSelector = document.querySelector('.j-langSelect')
  langSelector.value = zs.getLanguage()

  langSelector.addEventListener 'change', ()->
    lang = langSelector.value
    if lang == zs.getLanguage()
      return
    window.zs._setLanguageCookie lang
    route = window.location.pathname
    if route.length >= 3
      path = window.location.pathname
      if path.charAt(0) == '/' && path.charAt(3) == '/' && path.substr(1,2) in zs.getAvailableLanguages()
        route = route.substr(3)
    if lang != "de"
      route = "/#{lang}" + route
    window.location = route
    return false