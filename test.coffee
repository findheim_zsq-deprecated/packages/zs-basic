mongo = require './server/user/mongo'
mails = require './server/mails'

mongo.getUserByEmail "andreas.lackinger@zoomsquare.com", true
.then (user)->
  console.log "user loaded"
  mails.sendRegistrationComplete 'de', user
  .then ()->
    console.log "done"
  .catch (err)->
    console.log "could not send mail: ", err
.catch (err)->
  console.log "could not load user: ", err