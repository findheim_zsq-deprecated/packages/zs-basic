
class Statistics
  constructor: ()->
    @_statistics = {}

  reset: ()->
    @_statistics = {}
    
  get: (key) ->
    if key?
      return @_statistics[key]
    else
      return @_statistics

  set: (key, value) ->
    @_statistics[key] = value

  addCounter: (key) ->
    @_statistics[key] = 0

  incrementCounter: (key, value) ->
    unless @_statistics[key]?
      @_statistics[key] = 0
    unless value?
      value = 1
    @_statistics[key] += value

  addArray: (key) ->
    @_statistics[key] = []

  pushToArray: (key, value) ->
    unless @_statistics[key]?
      @_statistics[key] = []
    @_statistics[key].push(value)

  setGroupKey: (group, key, value)->
    unless @_statistics[group]?
      @_statistics[group] = {}
    @_statistics[group][key] = value

  incrementGroupCounter: (group, key, value) ->
    unless @_statistics[group]?
      @_statistics[group] = {}
    unless @_statistics[group][key]?
      @_statistics[group][key] = 0
    unless value?
      value = 1
    @_statistics[group][key] += value

module.exports = new Statistics()