cfg = require 'zs-app/lib/config'
mailServiceCfg = cfg.get('servers').mailService
logger = require('zs-logging').getLogger()
q = require 'q'
request = require 'request'

class Emails
  sendResetPassword: (country, user, app)->
    def = q.defer()
    payload =
      template: "passwordReset"
      email: user.emails[0].address
      country: country
    options =
      url: mailServiceCfg.url + mailServiceCfg.endpoints.sendUser
      method: 'POST'
      json: true
      body: payload
    if mailServiceCfg.auth.pass?.length > 0
      options.auth =
        user: mailServiceCfg.auth.user
        pass: mailServiceCfg.auth.pass
        sendImmediately: true
    request options, (err, msg, body)->
      if err?
        console.log err
        logger.error "error sending password reset email", {message: err.message, contactAgentRequestBody: payload}
        def.reject err
      else
        if msg.statusCode == 200
          logger.notice "sent password reset email", {contactAgentRequestBody: payload}
          def.resolve()
        else
          logger.error "error sending password reset email", {message: err?.message, contactAgentRequestBody: payload}
          def.reject err
    return def.promise

  sendRegistrationComplete: (country, user, app)->
    def = q.defer()
    payload =
      template: "emailVerification"
      email: user.emails[0].address
      country: country
    options =
      url: mailServiceCfg.url + mailServiceCfg.endpoints.sendUser
      method: 'POST'
      json: true
      body: payload
    if mailServiceCfg.auth.pass?.length > 0
      options.auth =
        user: mailServiceCfg.auth.user
        pass: mailServiceCfg.auth.pass
        sendImmediately: true
    request options, (err, msg, body)->
      if err?
        console.log err
        logger.error "error sending email verification", {message: err.message, contactAgentRequestBody: payload}
        def.reject err
      else
        if msg.statusCode == 200
          logger.notice "sent email verification", {contactAgentRequestBody: payload}
          def.resolve()
        else
          logger.error "error sending email verification", {message: err?.message, contactAgentRequestBody: payload}
          def.reject err
    return def.promise

module.exports = new Emails()