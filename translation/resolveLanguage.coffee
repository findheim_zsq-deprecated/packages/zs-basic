acceptLanguageParser = require 'accept-language-parser'
url = require 'url'
_ = require 'lodash'
cfg = require 'zs-app/lib/config'
country = cfg.get 'country'
languages = cfg.get 'languages'
countries = cfg.get 'countries'

module.exports = (req, res, next)->
  pathName = url.parse(req.originalUrl, true).pathname
  excludedPaths = ['/assets', '/favicon', '/api', '/map', '/robots.txt', '/sitemap', '/detail/assets', '/detail/sitemap', '/original', '/detail/api']
  for path in excludedPaths
    if pathName.match new RegExp("^#{path}")
      next()
      return

  if req.cookies.zsLang? and req.cookies.zsLang in languages
    chosenLanguage = req.cookies.zsLang
  else
# get visitor's language
    acceptLanguages = acceptLanguageParser.parse req.headers['accept-language']
    chosenLanguage = _.find(acceptLanguages, (o)-> return o.code in languages)?.code or countries[country].language

  res.cookie 'zsLang', chosenLanguage, {maxAge: 365*24*60*60*1000}

  # get current language from url
  for lang in languages
    if pathName.match new RegExp("^/#{lang}/")
      currentLanguage = lang
  currentLanguage = currentLanguage or countries[country].language

  if currentLanguage == chosenLanguage
    next()
    return
  else
    if chosenLanguage == countries[country].language
      res.redirect 302, req.originalUrl.slice 3
      return
    else
      res.redirect 302, "/#{chosenLanguage}" + req.originalUrl
      return