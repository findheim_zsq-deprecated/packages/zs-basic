_translations = ###translations###
_translationsLocationType = ###translationsLocationType###
_countries = ###availableCountries###


window.zs = window.zs or {}
window.zs.getLanguage = ()->
  return "###language###"
window.zs.getAvailableLanguages = ()->
  return ###availableLanguages###
window.zs.getCountryLanguage = ()->
  return "###countryLanguage###"
window.zs.getCountry = ()->
  return "###country###"
window.zs.translate = (key, fallback)->
  text = _translations[key] or fallback or ""
  if arguments.length > 2
    for i in [1..(arguments.length-2)]
      r = new RegExp("\\$#{i}", "g")
      text = text.replace new RegExp("\\$#{i}", "g"), arguments[i+1]
  return text
window.zs.translateLocationType = (location)->
  if location.city 
    return _translationsLocationType['c'] or location.typeName or ""
  else
    return _translationsLocationType[location.type] or location.typeName or ""

window.zs.getCountryName =  (country) ->
  country = window.zs.getCountry() if not country?
  # the country has to have a country entry in the config & has to have at least its name in english. as this is the fallback.
  name = _countries[country]?[window.zs.getLanguage()].countryName
  name = _countries[country]?['en'].countryName if not name?
  return name

window.zs.getCountryDomain = (country) ->
  country = window.zs.getCountry() if not country?
  return 'www.zoomsquare.' + _countries[country].topDomain

   
window.zs._setLanguageCookie = (lang)->
  date = new Date()
  date.setTime(date.getTime() + (365 * 24 * 60 * 60 * 1000))
  document.cookie = "zsLang=#{lang}; expires=#{date.toGMTString()}; path=/"
window.zs._removeLanguageCookie = ()->
  document.cookie = "zsLang=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/"
  
  
   
