try
  gulp = require "gulp"
catch err
  pRequire = require 'parent-require'
  gulp = pRequire "gulp"
$ = require('gulp-load-plugins')
  pattern: ['gulp-*']
tap = require "gulp-tap"
merge = require 'merge-stream'
translation = require("./index")
cfg = require 'zs-app/lib/config'
languages = cfg.get 'languages'
countries = cfg.get 'countries'
mongo = require './mongo'

langPath = (lang)->
  if lang == 'de'
    return ''
  else
    return "/#{lang}"

module.exports = (options, fileSource)->
  gulp.task "translate", (done)->
    files = []
    gulp.src fileSource
    .pipe tap (file)->
      files.push(file)
    .on 'end', ()->
      translation.extract(options.project, options.src, files)
      .finally ()->
        done()
    return

  gulp.task "createZsTranslation", (done)->
    country = cfg.get 'country'
    countries = cfg.get 'countries'
    countryLanguage = countries[country].language
    mongo.getTranslationsForCoffee options.project
    .then (translations)->
      streams = []
      languages.forEach (lang)->
        trans = {}
        for t in translations
          trans[t.key] = t.translations?[lang]?.text or t.default or ""
        stream = gulp.src "node_modules/web-basic/translation/zstranslation.coffee"
        .pipe $.change (content)->
          content = content.replace /###translations###/g, JSON.stringify(trans)
          content = content.replace /###translationsLocationType###/g, JSON.stringify(translation.locationTypes.getAll(lang))
          content = content.replace /###language###/g, lang
          content = content.replace /###availableLanguages###/g, JSON.stringify(languages)
          content = content.replace /###availableCountries###/g, JSON.stringify(countries)
          content = content.replace /###country###/g, country
          content = content.replace /###countryLanguage###/g, countryLanguage
          return content
        .pipe $.coffeelint()
        .pipe $.coffeelint.reporter()
        .pipe $.coffee().on 'error', (err)-> options.errorHandler("compile zsTranslation", err)
        .pipe $.uglify().on 'error', (err)-> options.errorHandler("Minify zsTranslation", err)
        .pipe gulp.dest("#{options.dest.root}#{langPath(lang)}#{options.dest.path}/assets/js/")
        streams.push stream
      merge(streams).on('end', done).on('data', ()-> return) #just read to empty it
    .catch (err)->
      options.errorHandler("createZsTranslation", err)
    return 
    
      
