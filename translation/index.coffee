mongo = require './mongo'
utils = require './utils'
q = require 'q'
async = require 'async'
chalk = require 'chalk'
_ = require 'lodash'
request = require 'request'
cfg = require 'zs-app/lib/config'
path = require 'path'

regexHtml = /{{\s*translate\s*(['"])(.*?)\1\s*(?:(['"])([\s\S]*?)\3)?\s*([\s\S]*?)\s*}}/gi
regexCoffee = /zs.translate\s*\(?\s*(["'])(.*?)\1\s*(?:,\s*(['"])(.*?)\3\s*)?(?:,\s*(.*)\s*)?\)?/g
regexParam = /(['"])(.*?)\1/gi

extractParams = (parameters)->
  result = []
  if parameters?.length > 0
    while param = regexParam.exec(parameters)
      result.push param[2]
  return result

class Translator
  constructor: ()->
    @translations = {}
    @missingTranslations = {de: [], en: []}

  init: (project)->
    def = q.defer()
    mongo.getTranslations(project, true)
    .then (translations)=>
      @translations = translations
      def.resolve()
    .catch (err)->
      def.reject err
    def.promise

  translate: (key, language, params)->
    unless @translations[key]?
      @missingTranslations[language].push key
      return
    if language == 'de'
      result = @translations[key].default
    else
      unless @translations[key].translations?[language]?.text?
        @missingTranslations[language].push key
        return @translations[key].default
      result = @translations[key].translations[language].text
    if _.isArray params
      for param, index in params
        result = result.replace(new RegExp("\\$#{index}","gi"), param)
    return result

  translateText: (text, language)->
    if language == 'de'
      return text.replace regexHtml, (match, a, key, b, defaultText, params)=>
        unless @translations[key]?.default?
          @missingTranslations['de'].push key
          return ""
        text = @translations[key].default
        for param, index in extractParams(params)
          text = text.replace(new RegExp("\\$#{index}","gi"), param)
        return text
    else
      return text.replace regexHtml, (match, a, key, b, defaultText, params)=>
        if @translations[key]?.translations?[language]?.text?
          text = @translations[key].translations[language].text
        else
          @missingTranslations[language].push key
          text = defaultText or ""
        for param, index in extractParams(params)
          text = text.replace(new RegExp("\\$#{index}","gi"), param)
        return text

  getMissing: ()->
    return @missingTranslations

class Extractor
  extract: (project, rootPath, files)->
    extractBasic = project == "web-basic"
    @printedError = false
    def = q.defer()
    stats = {counts:{}}
    stats[project] = {added: 0, changed: 0, removed: 0}
    stats["web-basic"] = {added: 0, changed: 0}
    updateMongo = true
    newTrans = {}
    console.log chalk.blue("Extractor") + ": " + "Extraction start"
    for file in files
      location = file.path.substring(file.path.indexOf(rootPath)).replace(/\\/g,'/') #normalized relative path
      console.log location
      extractedTranslations = @_extractFile file
      for result in extractedTranslations
        key = result[2]
        text = result[4]
        if newTrans[key]?
          if newTrans[key].default != text
            if text?
              if newTrans[key].default?
                @_printError "Duplicate key with different text", key
                updateMongo = false
              else
                newTrans[key].default = text
                newTrans[key].location.push
                  project: project
                  file: location
                  line: lines.length
                  column: lines[lines.length-1].length
          else
            if !_.isEqual newTrans[key].parameters, extractParams(result[5])
              @_printError "Duplicate key with different parameters", key
              updateMongo = false
            else
              lines = file.contents.toString().substring(0, result.index).split('\n')
              newTrans[key].location.push
                project: project
                file: location
                line: lines.length
                column: lines[lines.length-1].length
        else
          lines = file.contents.toString().substring(0, result.index).split('\n')
          newTrans[key] =
            key: key
            default: text
            location: [
              project: project
              file: location
              line: lines.length
              column: lines[lines.length-1].length
            ]
            parameters: extractParams(result[5])
    unless updateMongo
      @_printError "Extraction", "Error occured while extraction."
      def.reject new Error("Error occured while extraction.")
      return def.promise
    console.log chalk.blue("Extractor") + ": " + "Extraction finished"
    promises = [mongo.getTranslations(project)]
    unless extractBasic
      promises.push mongo.getTranslations("web-basic")
    q.all(promises).spread (oldTrans, basicTrans)=>
      try
        console.log chalk.blue("Extractor") + ": " + "Merge & Update start"
        stats.counts.extracted = utils.getObjectLength newTrans
        stats.counts.old = utils.getObjectLength(oldTrans, (val)-> return val.status != "unused")
        stats.counts.new = 0
        unless extractBasic
          stats.counts.basic = {existing: utils.getObjectLength(basicTrans), new: 0, used: 0}
        async.forEachOfSeries newTrans, (item, key, done)=>
          if oldTrans[key]?
            stats.counts.new++
            newItem = @_changeTranslation oldTrans[key], item
            if newItem?
              mongo.updateTranslation newItem
              .then ()->
                stats[project].changed++
                delete oldTrans[key]
                done()
              .catch (err)->
                done err
            else
              delete oldTrans[key]
              setTimeout done, 0
          else
            if !extractBasic && basicTrans[key]?
              stats.counts.basic.used++
              @_updateBasicTrans project, basicTrans[key], item
              .then (changed)->
                if changed
                  stats["web-basic"].changed++
                delete basicTrans[key]
                done()
              .catch (err)->
                done err
            else
              if extractBasic || item.default?.length > 0
                stats.counts.new++
                mongo.addTranslation project, item
                .then ()->
                  stats[project].added++
                  done()
                .catch (err)->
                  done err
              else
                stats.counts.basic.new++
                mongo.addTranslation "web-basic", item
                .then ()->
                  stats["web-basic"].added++
                  done()
                .catch (err)->
                  done err
        , (err)->
          if err?
            @_printError "Error occured while updating new translations", err.message
            def.reject err
          else
            console.log chalk.blue("Extractor") + ": " + "Merge & Update finished"
            unless extractBasic
              console.log chalk.blue("Extractor") + ": " + "Cleanup start"
              async.forEachOfSeries oldTrans, (item, key, done)->
                if item.status != "unused"
                  item.status = "unused"
                  mongo.updateTranslation item
                  .then ()->
                    stats[project].removed++
                    done()
                  .catch (err)->
                    done err
                else #unchanged
                  setTimeout done, 0
              , (err)->
                if err?
                  @_printError "Error occured while updating old translations", err.message
                  def.reject err
                else
                  console.log chalk.blue("Extractor") + ": " + "Cleanup finished"
                  console.log chalk.green("Extract complete") + ": " + JSON.stringify(stats, null, 2)
                  def.resolve()
            else
              console.log chalk.green("Extract complete") + ": " + JSON.stringify(stats, null, 2)
              def.resolve()
      catch err
        @_printError "Uncaught exception", err.message
    .catch (err)->
      @_printError "Error occured while loading translations", err.message
      def.reject err
    def.promise

  _extractFile: (file)->
    if path.extname(file.path) == '.html'
      return @_extractHtml(file.contents)
    else if path.extname(file.path) == '.coffee'
      return @_extractCoffee(file.contents)
    else
      return []
    
  _extractHtml: (contents)->
    res = []
    while item = regexHtml.exec(contents)
      res.push item
    return res
    
  _extractCoffee: (contents)->
    res = []
    while item = regexCoffee.exec(contents)
      res.push item
    return res
    
  _changeTranslation: (oldTrans, newTrans)->
    changed = false
    if oldTrans.status == "unused"
      changed = true
      oldTrans.status = "dirty"
    if newTrans.default? and oldTrans.default != newTrans.default
      changed = true
      oldTrans.default = newTrans.default
      for key, elem of oldTrans.translations
        elem.status = "dirty"
      oldTrans.status = "dirty"
    unless _.isEqual oldTrans.location, newTrans.location
      changed = true
      oldTrans.location = newTrans.location
    unless _.isEqual oldTrans.parameters, newTrans.parameters
      changed = true
      oldTrans.parameters = newTrans.parameters
    if changed
      return oldTrans
    return null

  _updateBasicTrans: (project, old, item)->
    def = q.defer()
    #check equality
    if item.default? && old.default != item.default
      def.reject new Error("used basic translation with key #{item.key}, but wrong default text")
      return def.promise
    if item.parameter? && !_.isEqual old.parameters, item.parameters
      def.reject new Error("used basic translation with key #{item.key}, but wrong parameters")
      return def.promise
    #update locations
    changed = false
    newLocations = []
    for loc in old.location
      if loc.project == project
        if utils.indexOf(item.location, loc) >= 0
          newLocations.push loc
        else
          changed = true
      else
        newLocations.push loc
    for loc in item.location
      unless utils.indexOf(newLocations, loc) >= 0
        newLocations.push loc
        changed = true
    #update mongo if changed    
    if changed
      old.location = newLocations
      mongo.updateTranslation old
      .then ()->
        def.resolve true
      .catch (err)->
        def.reject err
    else
      def.resolve false
    def.promise

  _printError: (title, message)->
    unless @printedError
      console.error chalk.red("ERROR") + " - " + chalk.yellow("Translator\n")
      @printedError = true
    console.error chalk.magenta(title) + ": " + message

class Types
  constructor: ()->
    @types = {}

  load: ()->
    def = q.defer()
    async.each cfg.get('languages'), (language, cb)=>
      request
        uri: "https://metadata.zoomsquare.com/xtr/metadata/getTypes?lang=#{language}"
        method: "GET"
        json: true
      , (err,msg,body)=>
        if err?
          cb(err)
        else
          types = {}
          for key, item of body
            types[key] = item.label
          @types[language] = types
          cb()
    , (err)->
      if err?
        def.reject err
      else
        def.resolve()
    def.promise

  translate: (key, language)->
    res = @types[language][key]
    if res?
      return res
    return key

class Properties
  constructor: ()->
    @properties = {}

  load: ()->
    def = q.defer()
    async.each cfg.get('languages'), (language, cb)=>
      request
        uri: "https://metadata.zoomsquare.com/xtr/metadata/getProperties?lang=#{language}"
        method: "GET"
        json: true
      , (err,msg,body)=>
        if err?
          cb(err)
        else
          properties = {}
          for key, item of body
            properties[key] = item.label
          @properties[language] = properties
          cb()
    , (err)->
      if err?
        def.reject err
      else
        def.resolve()
    def.promise

  translate: (key, language)->
    res = @properties[language][key]
    if res?
      return res
    else
      return key

extracor = new Extractor()
module.exports =
  extract: (project, rootPath, files)->
    extracor.extract(project, rootPath, files)
  translator: new Translator()
  types: new Types()
  properties: new Properties()
  locationTypes: require './translators/locationTypes'