# zoomsquare Translation service

## Translation tags
tags are enclosed in double curly brackets and start with the word translate, parameters are separated with space (handlebars style)  
{{translate "key" "text" "param 1" "param 2"}}
  
###Parameters
- key [mandatory]: this is the key of the translation
- text: the default german text for this translation  
    if this is missing, the text from the database is used  
    if this is missing during extraction and the key does not exits yet in the database, this will be extracted to the project 'web-basic'
- further parameters: every additional parameter can contain a text, which can be used in the text by using `$x`, where x is the number of the parameter  
    example: `{{translate "key" "$0strong$1 text" "<b>" "</b>"}}` will create the text `<b>strong</b> text`

## Extractor
The Extractor will extract all translateable text out of the project

### Usage
add this to your gulpfile: `require('web-basic/translation/gulp')(options, src)`

- options: [object] - necessary keys:
    - project: the unique name of the project 
    - src: the root path of the project
- src: [glob] which file to extract (example: `src/client/**/*.html`)

you can start the extraction with `gulp translate`  
this will extract everything, update the database and print a summary

## Translator