class LocationTypes
  constructor: ()->
    @types = 
      de:
        2: "Land"
        4: "Bundesland"
        6: "Bezirk"
        8: "Gemeinde"
        c: "Stadt"
        9: "Stadtbezirk"
        10: "Stadtteil"
      en:
        "2": "country"
        "4": "state"
        "6": "district"
        "8": "municipality"
        "c": "city"
        "9": "municipal district"
        "10": "neighborhood"

  translate: (loc, language)->
    if loc.city
      return @types[language]["c"] or loc.typeName or ""
    else
      return @types[language][loc.type] or loc.typeName or ""
    
  getAll: (language)->
    return @types[language] or {}
    
module.exports = new LocationTypes()