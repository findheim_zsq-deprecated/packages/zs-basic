_ = require 'lodash'

class Utils
  getObjectLength: (obj, check)->
    size = 0
    for key, val of obj
      if obj.hasOwnProperty(key)
        if !check? || check(val)
          size++
    return size
    
  indexOf: (array, item)->
    for elem, index in array
      if _.isEqual elem, item
        return index
    return -1
    
module.exports = new Utils