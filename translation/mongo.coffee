q = require("q")
try
  schemas = require 'zs-schemas'
catch
  schemas = require("parent-require") "zs-schemas"

Translations = schemas.model "Translations", "live"

class Mongo
  getTranslations: (project, includeBasic)->
    def = q.defer()
    qry = [project]
    if includeBasic?
      qry.push "web-basic"
    Translations.find({project: {$in: qry}}).lean().exec (err, res)->
      if err?
        def.reject err
      else
        result = {}
        for translation in res
          result[translation.key] = translation
        def.resolve result
    def.promise
    
  getTranslationsForCoffee: (project)->
    def = q.defer()
    Translations.find({location: {$elemMatch: {project: project, file: /\.coffee$/}}}).lean().exec (err, res)->
      if err?
        def.reject err
      else
        def.resolve res
    def.promise

  addTranslation: (project, translation)->
    def = q.defer()
    translation.project = project
    translation.status = "dirty"
    item = new Translations translation
    item.save (err)->
      if err?
        def.reject err
      else
        def.resolve item
    def.promise

  updateTranslation: (translation)->
    def = q.defer()
    unless translation._id?
      def.reject new Error("ID for update translation missing")
    translation.changed = new Date()
    Translations.findOneAndUpdate {_id: translation._id}, translation, {new: true}, (err, res)->
      if err?
        def.reject err
      else
        def.resolve res
    def.promise

module.exports = new Mongo()