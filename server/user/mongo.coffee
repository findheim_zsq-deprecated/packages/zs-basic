try
  schemas = require 'zs-schemas'
catch
  schemas = require("parent-require") "zs-schemas"

User = schemas.model "User", "live"
Searchprofile = schemas.model "SearchProfile", "live"
Leads = schemas.model "Leads", "live"
q = require 'q'
async = require 'async'
_ = require 'lodash'

# SH: i think this should all go into zs-schemas static methods
class Mongo
  selectedUserFields: # those should be called .*default*XFields
    createdAt: true
    emails: true
    profile: true
    "lists.faved": true
    "lists.trashed": true
    "createdUtm.source": true
  selectedSearchProfileFields:
    name: true
    isDefault: true
    createdAt: true
    searchQuery: true
    lists: true
    trefferupdate: true

  setDebug: (to = true) ->
    schemas.mongo.debug to

  getUserById: (id, includeServices, lean = true, includeStats)->
    def = q.defer()
    if id?
      fields = _.clone @selectedUserFields
      if includeServices
        fields.services = true
      if includeStats
        fields['stats.login'] = true
        fields['lists.viewed'] = true
      query = User.findOne({"_id": id}, fields)
      if lean
        query.lean()
      query.exec (err, res)->
        if err?
          def.reject err
        else
          def.resolve res
    else
      def.reject new Error "No id given"
    def.promise

  getUserByExternalId: (id, fields)->
    def = q.defer()
    if id?
      fields ?= _.clone @selectedUserFields
      query = User.findOne({"externalId": id}, fields)
      query.exec (err, res)->
        if err?
          def.reject err
        else
          def.resolve res
    else
      def.reject new Error "No id given"
    def.promise

  getUserByEmail: (email, includeServices)->
    def = q.defer()
    if email?
      fields = _.clone @selectedUserFields
      if includeServices
        fields.services = true
      User.findOne({"emails.address": email.toLowerCase()},fields).lean().exec (err, res)->
        if err?
          def.reject err
        else
          def.resolve res
    else
      def.reject new Error("No email given")
    def.promise

  getUserByFacebook: (profileId)->
    def = q.defer()
    unless profileId
      def.reject new Error "No Profile Id given"
      return def.promise
    fields = _.clone @selectedUserFields
    User.findOne({"services.facebook.id": profileId},fields).lean().exec (err, res)->
      if err?
        def.reject err
      else
        def.resolve res
    def.promise

  createUser: (app, country, language, email, password = null, name = null, searchQuery = null, profile = null, accessToken = null, data = null)->
    def = q.defer()
    # _basic_ checks
    unless _.isString app
      def.reject new Error "app not a string"
    unless _.isString country
      def.reject new Error "country not a string"
    unless _.isString language
      def.reject new Error "language not a string"
    unless _.isString email
      def.reject new Error "email not a string"

    q.all [
      @_createId('user')
      @_createId('searchprofile')
      @_createInviteId()
    ]
    .spread (userId, searchprofileId, inviteId)=>
      email = email.toLowerCase()

      userObject =
        _id: userId
        createdAt: new Date()
        createdLabel: app
        createdCountry: country
        emails: [
          address: email
          verified: false
        ]
        profile:
          inviteId: inviteId
          language: language
          searchprofiles: [
            searchprofileId
          ]
        lists: {faved:[], trashed:[]}

      if data?.contactData
        userObject.profile.contactData = data.contactData

      if data?.utm?
        userObject.createdUtm = 
          source: data.utm.source
          medium: data.utm.medium
          campaign: data.utm.campaign
          term: data.utm.term
          content: data.utm.content

      if profile? and accessToken? # create fb user
        userObject.emails[0].verified = true
        userObject.profile.firstname = profile.first_name
        userObject.profile.name = profile.name
        userObject.services =
          facebook:
            accessToken: accessToken.access_token
            id: profile.id
            email: profile.email
            name: profile.name
            first_name: profile.first_name
            last_name: profile.last_name
            link: profile.link
            gender: profile.gender
            locale: profile.locale
      else # create regular user with email and pw
        possibleChars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijlkmnopqrstuvwxyz"
        token = ""
        for i in [1..50]
          token += possibleChars.charAt(Math.floor(Math.random() * possibleChars.length))
        userObject.services =
          password:
            reset:
              token: token
              email: email
              when: new Date()
              tokenType: 'verification'
        if name?
          userObject.profile.firstname = name
        if password?
          userObject.services.password.bcrypt = password
        else
          userObject.profile.nopwd = true
      
      user = new User userObject

      searchprofile = new Searchprofile
        _id: searchprofileId
        name: "Mein Suchprofil"
        isDefault: true
        createdAt: new Date()
        country: country
        searchQuery: searchQuery

      searchprofile.save (err)=>
        if err
          def.reject err
        else
          user.save (err)=>
            if err
              def.reject err
            else
              def.resolve 
                user: user
                searchprofiles: [searchprofile]
    .catch (err)->
      def.reject(err)
    def.promise

  _createId: (collection, fieldName = '_id', length = 17)->
    def = q.defer()
    
    unless collection in ["user", "searchprofile"]
      def.reject "collection must be 'user' or 'searchprofile'"
      return def.promise
    if collection == 'user'
      db = User
    else
      db = Searchprofile

    newId = null
    possibleChars = "23456789ABCDEFGHJKLMNPQRSTWXYZabcdefghijkmnopqrstuvwxyz"
    tries = 0

    async.doUntil (callback)->
      tries++
      possibleId = []

      for i in [1..length]
        possibleId[i] = possibleChars.substr(Math.floor(Math.random() * possibleChars.length), 1)
      possibleId = possibleId.join("")

      db.findOne({fieldName: possibleId}, {fieldName: 1}).lean().exec (err, results)->
        if err
          callback err
        else
          unless results?
            newId = possibleId
          callback()
    , ()->
      return tries >= 9 or newId != null
    , (err)->
      if err?
        def.reject err
      else if newId?
        def.resolve newId
      else
        def.reject new Error("Could not create id within 10 tries")

    def.promise

  _createInviteId: ()->
    def = q.defer()

    inviteId = null
    possibleChars = "abcdefghijklmnopqrstuvwxyz0123456789"
    tries = 0

    async.doUntil (callback)->
      tries++
      possibleInviteId = ""
      for i in [1..4]
        possibleInviteId += possibleChars.charAt(Math.floor(Math.random() * possibleChars.length))
      User.findOne({'profile.inviteId': possibleInviteId}, {"_id": 1}).lean().exec (err, results)->
        if err
          callback err
        else
          unless results?
            inviteId = possibleInviteId
          callback()
    , ()->
      return tries >= 9 or inviteId != null
    , (err)->
      if err?
        def.reject err
      else if inviteId?
        def.resolve inviteId
      else
        def.reject new Error("Could not create inviteId within 10 tries")
    def.promise

  updateUserToFacebook: (userId, profile, accessToken)->
    def = q.defer()
    User.findOne {"_id": userId}, (err, user)->
      if err?
        def.reject err
      else
        user.services.facebook =
          accessToken: accessToken.access_token
          id: profile.id
          email: profile.email
          name: profile.name
          first_name: profile.first_name
          last_name: profile.last_name
          link: profile.link
          gender: profile.gender
          locale: profile.locale
        user.save (err, result)->
          if err?
            def.reject err
          else
            def.resolve result
    def.promise

  getSearchprofilesById: (ids, fields, country)->
    fields ?= _.clone @selectedSearchProfileFields
    def = q.defer()
    if ids?
      Searchprofile.find({"_id": {"$in": ids}, country: country}, fields).sort({isDefault: -1, name: 1}).lean().exec (err, res)->
        if err?
          def.reject err
        else
          def.resolve res
    else
      def.reject new Error("No profileIds given")
    return def.promise


  getSearchprofileCountriesById: (ids)->
    def = q.defer()
    if ids?
      Searchprofile.distinct('country',{"_id": {"$in": ids}}).lean().exec (err, res)->
        if err?
          def.reject err
        else
          def.resolve res
    else
      def.reject new Error("No profileIds given")
    return def.promise
    
  addSearchprofileToUser: (userId, country)->
    def = q.defer()
    try
      @_createId('searchprofile')
      .then (profileId)->
        searchprofile = new Searchprofile
          _id: profileId
          name: "Mein Suchprofil"
          country: country
          createdAt: new Date()
        searchprofile.save (err)=>
          if err
            def.reject err
          else
            User.update {"_id": userId}, {$push: {"profile.searchprofiles": profileId}}, (err, res)->
              if err
                def.reject err
              else
                def.resolve searchprofile
      .catch (err)->
        def.reject err
    catch err
      def.reject err
    def.promise

  getInviteId: (userId) ->
    deferred = q.defer()
    User.findOne {"_id": userId}, 'profile.inviteId', (err, user) ->
      if (err?)
        deferred.reject err
      else
        if user.profile.inviteId?
          deferred.resolve user.profile.inviteId
        else
          deferred.reject new error("user doesn't have an inviteId")
    deferred.promise

  getSearchprofilesByUser: (userId, country)->
    def = q.defer()
    unless userId?
      def.reject new Error "No userId given"
      return def.promise
    User.findOne({"_id": userId}, {"profile.searchprofiles": 1}).lean().exec (err, user)=>
      if err?
        def.reject err
      else
        if user?
          @getSearchprofilesById user.profile.searchprofiles, null, country
          .then (profiles)->
            def.resolve profiles
          .catch (err)->
            def.reject err
        else
          def.reject new Error("invalid userId")
    def.promise

  getSearchprofileCountriesByUser: (userId) ->
    def = q.defer()
    unless userId?
      def.reject new Error "No userId given"
      return def.promise
    User.findOne({"_id": userId}, {"profile.searchprofiles": 1}).lean().exec (err, user)=>
      if err?
        def.reject err
      else
        if user?
          @getSearchprofileCountriesById user.profile.searchprofiles
          .then (countries)->
            def.resolve countries
          .catch (err)->
            def.reject err
        else
          def.reject new Error("invalid userId")
    def.promise

  resetPassword: (email)->
    def = q.defer()
    User.findOne {"emails.address": email}, (err, user)->
      if err?
        def.reject err
      else if !user?
        def.reject 
          code: 1
          message: 'User does not exist'
      else
        possibleChars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijlkmnopqrstuvwxyz"
        token = ""
        for i in [1..50]
          token += possibleChars.charAt(Math.floor(Math.random() * possibleChars.length))
        user.services.password.reset =
          token: token
          email: email
          when: new Date()
        user.save (err, user)->
          if err?
            def.reject err
          else
            def.resolve user
    def.promise

  setFirstNameAndPersonId: (userId, firstName, personId)->
    def = q.defer()
    User.findOneAndUpdate {"_id": userId}, {$set: {"profile.firstname": firstName, "profile.personId": personId}}, {new: true, fields: @selectedUserFields}, (err, user)->
      if err
        def.reject err
      else
        def.resolve user
    def.promise
    
  setPersonId: (userId, personId)->
    def = q.defer()
    User.findOneAndUpdate {"_id": userId}, {$set: {"profile.personId": personId}}, {new: true, fields: @selectedUserFields}, (err, user)->
      if err
        def.reject err
      else
        def.resolve user
    def.promise

  checkToken: (token)->
    def = q.defer()
    User.findOne {"services.password.reset.token": token}, (err, user)->
      if err?
        def.reject err
        return
      unless user?
        def.resolve
          ok: false
          message: 'invalid token'
        return

      if user.services.password.reset.tokenType == 'verification'
        type = 'verification'
      else
        type = 'reset'

      dirty = false
      unless user.emails[0].verified
        dirty = true
        user.emails[0].verified = true
      if type == 'verification'
        dirty = true
        user.services.password.reset = null

      if dirty
        user.save (err, user)->
          if err?
            def.reject err
          else
            def.resolve
              ok: true
              type: type
              user: user
      else
        def.resolve
          ok: true
          type: type
          user: user
    def.promise
    
  setPassword: (userId, newPassword)->
    def = q.defer()
    User.findOne {"_id": userId}, (err, user)->
      if err?
        def.reject err
        return def.promise
      unless user?
        def.reject 'no user'
        return def.promise

      user.services.password.bcrypt = newPassword
      user.services.password.reset = null

      user.save (err, user)->
        def.resolve user
    def.promise
    
  saveContactData: (id, data)->
    unless id?
      q.reject new Error("user id missing")
    data = data or {}
    query =
      "$set":
        "profile.contactData":
          firstName: data.firstName
          lastName: data.lastName
          email: data.email
          phone: data.phone
          message: data.message
          terms: data.terms
    User.update({"_id": id}, query).exec()

  addListViewed: (userId, extId)->
    unless userId?
      return q.reject new Error("No userId given")
    unless extId?
      return q.reject new Error("No extId given")

    def = q.defer()
    User.update({"_id": userId}, {"$push": {"lists.viewed": {"extId": extId, "date": new Date()}}}).exec (err, res)->
      if err?
        def.reject err
      else if res.nModified != 1
        def.reject new Error("User update without error, but affected no documents")
      else
        def.resolve()
    def.promise

  # TODO remove
  addListLead: (userId, extId, leadType)->
    unless userId?
      return q.reject new Error("No userId given")
    unless extId?
      return q.reject new Error("No extId given")
    unless leadType in ["premium", "listing", "external"]
      return q.reject new Error("No or invalid leadType given")

    def = q.defer()
    User.update({"_id": userId}, {"$push": {"lists.lead": {"extId": extId, "date": new Date(), type: leadType}}}).exec (err, res)->
      if err?
        def.reject err
      else if res.nModified != 1
        def.reject new Error("User update without error, but affected no documents")
      else
        def.resolve()
    def.promise

  saveLead: (type, extId, userId, searchQuery = null)->
    unless userId?
      return q.reject new Error("No userId given")
    unless extId?
      return q.reject new Error("No extId given")
    unless type in ["premium", "listing", "external"]
      return q.reject new Error("No or invalid type given")

    leadObject = {}
    leadObject.createdAt = new Date()
    leadObject.type = type
    leadObject.extId = extId
    leadObject.userId = userId
    leadObject.searchQuery = searchQuery

    lead = new Leads leadObject
    lead.save()

  updateWizardData: (id, data)->
    unless id?
      q.reject new Error("user id missing")
    data = data or {}
    query =
      "$set":
        "profile.wizardData": data
    User.update({"_id": id}, query).exec()

  checkExternalId: (id)->
    def = q.defer()
    unless id?
      def.reject new Error("user id missing")
      return def.promise
    User.findOne {"_id": id}, (err, user)=>
      if err?
        def.reject err
        return def.promise
      unless user?
        def.reject 'no user'
        return def.promise
      if user.externalId?.length > 0
        def.resolve()
      else
        @_createId 'user', 'externalId', 40
        .then (newExternalId)->
          User.update({"_id": id}, {"$set": {"externalId": newExternalId}}).exec()
    def.promise

  updateUseProfileData: (id, doNotUseProfileData)->
    unless id?
      q.reject new Error("user id missing")
    query =
      "$set":
        "profile.doNotUseProfileData": doNotUseProfileData
    User.update({"_id": id}, query).exec()

module.exports = new Mongo()