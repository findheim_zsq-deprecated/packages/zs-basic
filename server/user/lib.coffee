q = require 'q'
mongo = require './mongo'
crypto = require "crypto"
bcrypt = require "bcrypt"
request = require 'request'
validator = require 'validator'
logger = require('zs-logging').getLogger()
cfg = require 'zs-app/lib/config'
fbCfg = cfg.get 'facebook'
userServiceCfg = cfg.get("userService")
rootUrls = cfg.get 'rootUrls'
mailService = require './../../mails'

# class for all public user functions
# all function are async and will return a promise if return data is expected from the function
# promises will be resolved on success with an object containing 2 more object:
#   user [object] the user object containing '_id', 'createdAt', 'emails', 'profile'
#   searchprofiles [array] an array of all searchprofiles for this user, each searchprofile contains 'name', 'isDefault', 'createdAt', 'searchQuery'
class Lib
  # Perform a login for an user
  # This function works async and will return a promise
  #
  # @param email [String] the email address of the user
  # @param password [String] the password of the user
  #
  # @return [Object] promise
  #   on success: will be resolved with an object containing the keys 'user' and 'searchprofiles'
  #   on error: will be rejcted with an error (containing a message with more info)
  login: (email, password, country)->
    def = q.defer()
    if email? and password? and country?
      mongo.getUserByEmail email, true
      .then (user)->
        unless user?
          return def.reject new Error('user_not_found')
        unless user.services.password?.bcrypt?
          if user.services.facebook?
            return def.reject new Error('user_facebook_account')
          else
            return def.reject new Error('user_password_not_set')
    
        shaHash = crypto.createHash 'sha256'
        shaHash.update password
        hashedPassword = shaHash.digest('hex')
        unless bcrypt.compareSync(hashedPassword, user.services.password.bcrypt)
          return def.reject new Error('wrong_pass')
    
        delete user.services # do not return this precious data
    
        mongo.getSearchprofilesById user.profile.searchprofiles, null, country
        .then (searchprofiles)->
          unless searchprofiles?.length > 0
            mongo.addSearchprofileToUser user._id, country
            .then (profile)->
              # the user object doesnt have this set
              user.profile.searchprofiles.push profile._id
              def.resolve
                user: user
                searchprofiles: [profile]
            .catch (err)->
              def.reject err
          else
            def.resolve
              user: user
              searchprofiles: searchprofiles
        .catch (err)->
          def.reject err
      .catch (err)->
        def.reject err
    else
      def.reject new Error "Email, password and country are required"
    def.promise

  # Perform a registration for an user
  # This function works async and will return a promise
  #
  # @param app [String] the identifier of the app, which calls this function (web3, app)
  # @param country [String] the country, for which this user is registered (at, de) WARNING: at is not yet fully implemented, there are email templates missing
  # @param language [String] the selected language for the user, necessary to send the correct registration complete email (de, en), WARNING: en email template missing
  # @param email [String] the desired email for this user
  # @param password [String] optional - the desired password for this user; if omitted, the user can't login, until he sets his password
  # @param name [String] optional - the desired name for this user; can be set later, when the user sets his password
  # @param searchQuery [object] optional - a searchQuery for this user, will be stored in the default searchprofile if given
  #
  # @return [Object] promise
  #   on success: will be resolved with an object containing the keys 'user' and 'searchprofiles'
  #   on error: will be rejcted with an error (containing a message with more info)
  register: (app, country, language, email, password, name, searchQuery, data)->
    def = q.defer()
    if email? and validator.isEmail(email)
      mongo.getUserByEmail email
      .then (user)->
        if user?
          def.reject new Error('email_exists')
        else
          if password?
            shaHash = crypto.createHash 'sha256'
            shaHash.update password
            hashedPassword = shaHash.digest('hex')
            salt = bcrypt.genSaltSync(10)
            password = bcrypt.hashSync(hashedPassword, salt)
          mongo.createUser app, country, language, email, password, name, searchQuery, null, null, data
          .then (result)->
            mailService.sendRegistrationComplete country, result.user, app
            # SH: this gets returned anyway. mongoose weirdness..
            delete result.user.services # do not return this precious data
            def.resolve result
          .catch (err)->
            def.reject err
      .catch (err)->
        def.reject err
    else
      def.reject new Error('email_not_valid')
    def.promise

  # Resends the registration complete email for a given user
  #
  # @param country [String] the country, for which this user is registered (at, de)
  # @param user [object] optional - a searchQuery for this user (will be ignored, if not registration)
  #
  # @return [Object] promise
  #   on success: will be resolved without data
  #   on error: will be rejcted with an error (containing a message with more info)
  resendRegistrationComplete: (country, user, app)->
    mailService.sendRegistrationComplete country, user, app

  resendRegistrationCompleteToUserId: (country, userId, app)->
    def = q.defer()
    mongo.getUserById userId
    .then (user)->
      mailService.sendRegistrationComplete country, user, app
      .then (result)->
        def.resolve result
      .catch (err)->
        def.reject err
    .catch (err)->
      def.reject err
    def.promise
    

  # Perform a facebook action for an user
  # depending on the given data and database entry, the action can be register, login or update
  # This function works async and will return a promise
  #
  # @param app [String] the identifier of the app, which calls this function (web3, app)
  # @param country [String] the country, for which this user is registered (at, de)
  # @param language [String] the selected language for the user, necessary to send the correct registration complete email (de, en)
  # @param code [String] code for the facebook api
  # @param redirect_uri [String] redirect_uri for the facebool api 
  # @param searchQuery [object] optional - a searchQuery for this user (will be ignored, if not registration)
  #
  # @return [Object] promise
  #   on success: will be resolved with an object containing the keys 'user' and 'searchprofiles'
  #   on error: will be rejcted with an error (containing a message with more info)
  facebook: (app, country, language, code, redirect_uri, searchQuery, data)->
    def = q.defer()
    params =
      code: code
      client_id: fbCfg.client_id
      client_secret: fbCfg.client_secret
      redirect_uri: redirect_uri
    request.get {url: fbCfg.endpoints.accessToken, qs: params, json: true}, (err, response, accessToken)->
      if err?
        return def.reject err
      unless response.statusCode == 200
        return def.reject new Error(accessToken.error.message)
      request.get {url: fbCfg.endpoints.me, qs: accessToken, json: true}, (err, response, profile)->
        if err?
          return def.reject err
        unless response.statusCode == 200
          return def.reject new Error(profile.error.message)

        mongo.getUserByFacebook(profile.id)
        .then (user)->
          if user?
            mongo.getSearchprofilesById user.profile.searchprofiles, null, country
            .then (searchprofiles)->
              unless searchprofiles?.length > 0
                mongo.addSearchprofileToUser user._id, country
                .then (profile)->
                  # the user object doesnt have this set
                  user.profile.searchprofiles.push profile._id
                  def.resolve
                    user: user
                    searchprofiles: [profile]
                .catch (err)->
                  def.reject err
              else
                def.resolve
                  user: user
                  searchprofiles: searchprofiles
            .catch (err)->
              def.reject err
          else
            mongo.getUserByEmail profile.email
            .then (user)->
              if user?
                mongo.updateUserToFacebook user._id, profile, accessToken
                .then (user)->
                  mongo.getSearchprofilesById user.profile.searchprofiles, null, country
                  .then (searchprofiles)->
                    unless searchprofiles?.length > 0
                      mongo.addSearchprofileToUser user._id, country
                      .then (profile)->
                        # the user object doesnt have this set
                        user.profile.searchprofiles.push profile._id
                        def.resolve
                          user: user
                          searchprofiles: [profile]
                      .catch (err)->
                        def.reject err
                    else
                      def.resolve
                        user: user
                        searchprofiles: searchprofiles
                  .catch (err)->
                    def.reject err
                .catch (err)->
                  def.reject err
              else
                mongo.createUser app, country, language, profile.email, null, null, searchQuery, profile, accessToken, data
                .then (result)->
                  delete result.user.services # do not return this precious data
                  def.resolve result
                .catch (err)->
                  def.reject err
            .catch (err)->
              def.reject err
        .catch (err)->
          def.reject err
    def.promise

  getSearchprofilesByUser: (userId, country)->
    def = q.defer()
    mongo.getSearchprofilesByUser userId, country
    .then (profiles)->
      if profiles?.length > 0
        def.resolve profiles
      else
        mongo.addSearchprofileToUser userId, country
        .then (profile)->
          def.resolve [profile]
        .catch (err)->
          def.reject err
    .catch (err)->
      def.reject err
    def.promise

  getSearchprofileCountriesByUser: (userId)->
    def = q.defer()
    mongo.getSearchprofileCountriesByUser userId
    .then (countries)->
      def.resolve countries
    .catch (err)->
      def.reject err
    def.promise

  resetPassword: (country, email, app)->
    def = q.defer()
    mongo.resetPassword(email)
    .then (user)->
      mailService.sendResetPassword(country, user, app)
      .then ()->
        def.resolve()
      .catch (err)->
        def.reject err
    .catch (err)->
      def.reject(err)
    def.promise

  setPersonId: (userId, personId)->
    mongo.setPersonId userId, personId

  setFirstNameAndPersonId: (userId, firstName, personId)->
    mongo.setFirstNameAndPersonId userId, firstName, personId
    
  getUserById: (userId, includeServices, includeStats)->
    mongo.getUserById userId, includeServices, true, includeStats

  checkToken: (token)->
    mongo.checkToken token

  addListViewed: (userId, extId)->
    mongo.addListViewed userId, extId

   # TODO remove
  addListLead: (userId, extId, leadType)->
    mongo.addListLead userId, extId, leadType
  
  saveLeadMongo: (type, extId, userId, searchQuery)->
    mongo.saveLead type, extId, userId, searchQuery

  saveLeadUserService: (data)->
    unless Object.keys(data).length >= 9
      logger.error "error sending lead to userService - not enough data", {leadData: data}
      return
    request.post
      url: "#{userServiceCfg.base}/person/#{data.personId}/generateLead"
      auth:
        user: userServiceCfg.user
        pass: userServiceCfg.pass
      json: true
      body:
        firstName: data.firstName
        lastName: data.lastName
        email: data.email
        phone: data.phone
        userId: data.userId
        country: data.country
        searchProfile: data.searchProfile
        comment: data.comment
        estateViewModified: data.estateViewModified
        extId: data.extId
        utm: data.utm
    , (err, response, lead)->
      if err or response.statusCode != 201
        logger.error "error sending lead to userService - status not 201", {errMessage: err?.message, statusCode: response.statusCode, leadData: data}
      else
        logger.error "lead sent to userService", {statusCode: response.statusCode, leadData: data}


  updateWizardData: (userId, data)->
    mongo.updateWizardData userId, data

  setPassword: (userId, token, newPassword)->
    def = q.defer()
    mongo.getUserById userId, true
    .then (user)->
      if user.services.password.reset.token == token
        shaHash = crypto.createHash 'sha256'
        shaHash.update newPassword
        hashedPassword = shaHash.digest('hex')
        salt = bcrypt.genSaltSync(10)
        newPassword = bcrypt.hashSync(hashedPassword, salt)
        mongo.setPassword userId, newPassword
        .then (user)->
          def.resolve user
        .catch (err)->
          def.reject err
      else
        def.reject 
          code: 1
          message: "invalid token"
    .catch (err)->
      def.reject err
    def.promise

  saveContactData: (userId, data)->
    mongo.saveContactData userId, data

  getCompleteUserByExternalId: (externalId, country)->
    def = q.defer()
    unless externalId? and country?
      def.reject new Error "No id or country given"
      return def.promise
    mongo.getUserByExternalId externalId
    .then (user)->
      unless user?
        return def.reject new Error('user_not_found')
      mongo.getSearchprofilesById user.profile.searchprofiles, {}, country
      .then (searchProfiles)->
        unless searchProfiles?.length > 0
          return def.reject new Error('searchprofile_not_found')
        def.resolve
          user: user
          searchProfile: searchProfiles[0]
      .catch (err)->
        def.reject(err)
    .catch (err)->
      def.reject(err)
    def.promise

  checkExternalId: (userId)->
    mongo.checkExternalId userId

  updateUseProfileData: (userId, doNotUseProfileData)->
    mongo.updateUseProfileData userId, doNotUseProfileData

  _aaa = ()->
    mongo.addSearchprofileToUser user._id, country
    .then (profile)->
# the user object doesnt have this set
      user.profile.searchprofiles.push profile._id
      def.resolve
        user: user
        searchprofiles: [profile]
    .catch (err)->
      def.reject err

module.exports = new Lib()