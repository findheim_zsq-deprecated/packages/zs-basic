cfg = require 'zs-app/lib/config'
infinity = cfg.get 'infinity'

class ProfileConverter

  ###
    Converts a search query back into a request that was initially sent by the client. If 'searchQuery' is null, an
    empty object is returned.
  this does not support features currently.
  ###
  convertToUrl: (searchQuery)->
    unless searchQuery?
      return {}

    url = {}
    type = searchQuery.type[0]
    ownType = if searchQuery.hasOwnProperty("rentalFee") then 0 else 1
    switch type
      when "Apartment"
        if ownType == 0 then url.t = '0' else url.t = '1'
      when "House"
        if ownType == 0 then url.t = '2' else url.t = '3'
      when "Plot" then url.t = '5'

    url.b = searchQuery.bbox[0]+','+searchQuery.bbox[1] if searchQuery.bbox?
    if searchQuery.geoFields?[0] == "geoEntities"
      url.m = '0'
      l = searchQuery.geoEntities.join ','
      url.l = l
    else
      url.m = '1'
      if searchQuery.geoEntities?
        l = searchQuery.geoEntities.join ','
        url.l = l

    if searchQuery.sort?
      for sort in searchQuery.sort
        switch sort.sortBy
          when "created" then url.s = 0
          when "_es.sort.price" then url.s = 1
          when "_es.sort.pricePerArea" then url.s = 2
          when "_es.sort.area" then url.s = 3
          when "props.rooms" then url.s = 4
    else
      url.s = 0

    price = searchQuery[if ownType == 0 then "rentalFee" else "purchasePrice"]
    if price?.length == 2
      if price[1] >= infinity
        price[1] = 'i'
      url.p = price[0]+','+price[1]

    space = searchQuery.livingSpace
    if space?.length == 2
      if space[1] >= infinity
        space[1] = 'i'
      url.a = space[0]+','+space[1]

    if searchQuery.hasOwnProperty("rooms")
      rooms = searchQuery.rooms
      url.r = rooms[0]

    if url.o? then url.o = (searchQuery._meta.offset / 20) + 1

    return url


module.exports = new ProfileConverter()