base32 = require './lib/base32.js'

class Utils
  getObjectLength: (obj)->
    size = 0
    for key of obj
      if obj.hasOwnProperty(key)
        size++
    return size

  decodeExtId: (extId)->
    unless extId?
      return
    if extId.indexOf('.') == -1
      extId = base32.decode extId
    return extId

  encodeExtId: (extId)->
    unless extId?
      return
    if extId.indexOf('.') != -1
      extId = base32.encode extId
    return extId

  prepError: (error, noWrap)->
    errObj = {}
    for prop in Object.getOwnPropertyNames(error)
      errObj[prop] = error[prop]
    if noWrap
      return errObj
    return {preppedError: errObj}
    
module.exports = new Utils()