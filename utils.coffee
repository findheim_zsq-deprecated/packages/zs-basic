class Utils
  getEstateSpaces: (props)->
    result = {}
    if "Room" in props.type
      if props.roomSpace?
        result.primary = {key: "roomSpace", value: props.roomSpace}
        if props.livingSpace?
          result.others = result.others or {}
          result.others.livingSpace = props.livingSpace
      else if props.livingSpace?
        result.primary = {key: "livingSpace", value: props.livingSpace}
    else if "Apartment" in props.type
      if props.livingSpace?
        result.primary = {key: "livingSpace", value: props.livingSpace}
      if props.usableSpace? && (!props.livingSpace? || props.usableSpace != props.livingSpace)
        result.others = result.others or {}
        result.others.usableSpace = props.usableSpace
    else if "House" in props.type
      if props.livingSpace?
        result.primary = {key: "livingSpace", value: props.livingSpace}
      if props.plotArea?
        result.others = result.others or {}
        result.others.plotArea = props.plotArea
      if props.usableSpace? && (!props.livingSpace? || props.usableSpace != props.livingSpace) && (!props.plotArea? || props.usableSpace != props.plotArea)
        result.others = result.others or {}
        result.others.usableSpace = props.usableSpace
    else if "Plot" in props.type
      if props.plotArea?
        result.primary = {key: "plotArea", value: props.plotArea}
      if props.usableSpace? && (!props.plotArea? || props.usableSpace != props.plotArea)
        result.others = result.others or {}
        result.others.usableSpace = props.usableSpace
    else if "Commercial" in props.type
      if props.usableSpace?
        result.primary = {key: "usableSpace", value: props.usableSpace}
    return result

module.exports = new Utils()