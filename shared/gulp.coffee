module.exports = (gulp, options)->
  basicsSrc = ["node_modules/web-basic/shared/#{options.country}/**/*"]
  jsSrc = ["node_modules/zs-map/build/leaflet.js"]
  cssSrc = ["node_modules/zs-map/build/leaflet.css"]
  
  gulp.task "assets-shared-basic", ->
    gulp.src basicsSrc
    .pipe gulp.dest("#{options.dest.root}/assets/shared/")

  gulp.task "assets-shared-js", ->
    gulp.src jsSrc
    .pipe gulp.dest("#{options.dest.root}/assets/shared/js/")

  gulp.task "assets-shared-css", ->
    gulp.src cssSrc
    .pipe gulp.dest("#{options.dest.root}/assets/shared/css/")

  gulp.task "assets-shared", ["assets-shared-basic","assets-shared-js","assets-shared-css"]